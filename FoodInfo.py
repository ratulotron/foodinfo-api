# -*- coding: utf-8 -*-
# all the imports
from flask import Flask, request, g, Response
import rethinkdb as r
import json


# Configurations
HOST        = 'localhost'
DATABASE    = 'test'
DEBUG       = True


app = Flask(__name__)
app.config.from_object(__name__)


@app.before_request
def before_request():
    g.db = r.connect(host=HOST, port=28015, db=DATABASE)


@app.teardown_request
def teardown_request(exception):
    g.db.close()


@app.route('/')
def index():
    return """<h1>go to the <a href = "/restaurants?status=">page</a></h1>"""

@app.route('/restaurants', methods=['get'])
def restaurant_data():
    status = request.args.get('status')
    # city = request.args.get('city')
    places = []
    for location in  r.table('restaurants').filter(r.row['status'] == status).run(g.db):
        places.append(location)
    return Response(json.dumps(places, sort_keys=True, indent=4, separators=(',', ': ')),
                               mimetype='application/json')

if __name__ == '__main__':
    app.run(host='0.0.0.0')
