# FoodInfo API

Demo API for FoodInfo app.

## Usage
### localhost
- Clone the repo: `git clone https://github.com/mnzr/FoodInfo-API.git`
- Install rethinkDB
- Import demo data: `rethinkdb restore -c HOST:PORT`
- Install dependencies: `pip install -r requirements.txt`
- Run the app: `python FoodInfo.py`
- Visit the app: `http://localhost:5000/restaurant?status=` 

### On Koding
- Just clone and run the damn app
- Visit the app: `http://minhazr.koding.io:5000/restaurant?status=`

### Parameters
- for status
    - open
    - closed

## TODO
- Add input boxes in homepage.
